<?php

// +----------------------------------------------------------------------
// | ThinkApiAdmin
// +----------------------------------------------------------------------

/**
 * Created by PhpStorm.
 * Author: zwt
 * Date: 2018/4/8
 * Time: 22:17
 * Email: zwt0706@gmail.com
 */

namespace app\port\controller;

use app\model\ApiList;
use controller\BasicAdmin;
use service\LogService;
use think\Db;

/**
 * Class Row
 * @package app\port\controller
 */
class Row extends BasicAdmin
{

    /**
     * 指定当前数据表
     * @var string
     */
    public $table_api_list = 'ApiList';    //api接口列表
    public $table_admin = 'SystemUser';      //系统用户表

    /**
     * 接口列表
     * @return array|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $this->title = '接口列表';
        $get = $this->request->get();
        $db = Db::name($this->table_api_list)
            ->where('is_deleted', '0')
            ->order('id', 'desc');
        foreach (['apiName', 'hash', 'info'] as $key) {
            if (isset($get[$key]) && $get[$key] !== '') {
                $db->where($key, 'like', "%{$get[$key]}%");
            }
        }
        foreach (['handler', 'status'] as $key) {
            if (isset($get[$key]) && $get[$key] !== '') {
                $db->where($key, $get[$key]);
            }
        }
        return parent::_list($db);
    }

    /**
     * 列表数据处理
     * @param $list
     */
    protected function _index_data_filter(&$list)
    {
        $handlers = Db::name($this->table_admin)->column('id,username');
        foreach ($list as &$vo) {
            $vo['handler_name'] = Db::name($this->table_admin)->where('id', $vo['handler'])->value('username');
        }
        $this->assign(['handlers' => $handlers]);
    }

    /**
     * 刷新接口路由  保持固定格式
     * @throws \think\exception\DbException
     */
    public function refresh()
    {
        $apiRoutePath = ROOT_PATH . 'application/apiRoute.php';
        $tplPath = ROOT_PATH . 'application/util/apiRoute.tpl';
        $methodArr = ['*', 'post', 'get'];

        $tplStr = file_get_contents($tplPath);
        $listInfo = ApiList::all(['status' => 1]);
        // 保持固定格式
        $foot = PHP_EOL . '        // 接口Hash异常跳转' . PHP_EOL . '        \'__miss__\' => [\'api/Miss/index\'],' . PHP_EOL . '    ],' . PHP_EOL . '];';
        foreach ($listInfo as $value) {
            $tplStr .= '        // ' . $value->info . ' ' . $value->hash . PHP_EOL . '        \'' . $value->hash . '\' => [' . PHP_EOL . '            \'api/' . $value->apiName . '\',' . PHP_EOL . '            [\'method\' => \'' . $methodArr[$value->method] . '\', \'after_behavior\' => $afterBehavior]' . PHP_EOL . '        ],' . PHP_EOL;
        }
        file_put_contents($apiRoutePath, $tplStr . $foot);
        LogService::write('API接口管理', '刷新接口路由成功');
        $this->success('刷新接口路由成功!', '');
    }

}